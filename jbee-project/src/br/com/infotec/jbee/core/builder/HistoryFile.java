/***
 * Copyright (c) 2010 Infotec - http://sites.google.com/site/jbeetest/ All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package br.com.infotec.jbee.core.builder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Represents a history that was loaded from the file specified in the test class
 * 
 * @author Carlos Alberto (euprogramador@gmail.com)
 * @since 1.0
 */
public class HistoryFile {

	private List<Step> steps;

	public HistoryFile() {
		steps = new ArrayList<Step>();
	}

	public void addStep(Step step) {
		steps.add(step);
	}

	public List<Step> getSteps() {
		return Collections.unmodifiableList(steps);
	}
}
