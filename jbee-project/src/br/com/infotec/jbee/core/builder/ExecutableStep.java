/***
 * Copyright (c) 2010 Infotec - http://sites.google.com/site/jbeetest/ All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package br.com.infotec.jbee.core.builder;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import org.junit.Ignore;

import br.com.infotec.jbee.core.evaluation.Evaluation;

/**
 * Defines a step that can be executed
 * 
 * @author Carlos Alberto (euprogramador@gmail.com)
 * @since 1.0
 */
public class ExecutableStep implements Step {

	private String description;
	private String status = " (NOT PERFORMED)";

	public ExecutableStep(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean execute(Object instanceForTesting) throws Exception {
		try {
			Evaluation evaluation = new Evaluation();
			boolean isEquals;
			Class realClass = getRealClass(instanceForTesting);
			for (Method method : realClass.getMethods()) {
				br.com.infotec.jbee.core.annotations.Step stepAnnotation = method
						.getAnnotation(br.com.infotec.jbee.core.annotations.Step.class);
				br.com.infotec.jbee.core.annotations.Ignore stepIgnore = method
						.getAnnotation(br.com.infotec.jbee.core.annotations.Ignore.class);
				if (stepAnnotation == null) {
					continue;
				}
				
				for (String step_text : stepAnnotation.value()) {
					isEquals = evaluation.isEqualsStep(step_text.trim(), getDescription().trim());
					if (isEquals) {
						if (stepIgnore != null) {
							status = " (Ignored)";
							return true;
						} else {
							method.invoke(instanceForTesting, evaluation.getArguments(step_text, getDescription()));
							status = "";
							return true;
						}
					}
				}
				
			}
			status = " (Pending)";
			return false;
		} catch (Exception e) {
			status = " (fail)";
			throw e;
		}
	}

	@SuppressWarnings("rawtypes")
	private Class getRealClass(Object instanceForTesting) throws ClassNotFoundException {
		int pos = instanceForTesting.getClass().getCanonicalName().indexOf("$");
		String className;
		if (pos > 0) {
			className = instanceForTesting.getClass().getCanonicalName().substring(0, pos);
		} else {
			className = instanceForTesting.getClass().getCanonicalName();
		}
		return Class.forName(className);
	}

	@Override
	public void printDescription() {
		System.out.print(description);
	}

	@Override
	public void printStatus() {
		System.out.println(status);

	}

}
