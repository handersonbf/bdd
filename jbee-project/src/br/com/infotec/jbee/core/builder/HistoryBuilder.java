/***
 * Copyright (c) 2010 Infotec - http://sites.google.com/site/jbeetest/ All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package br.com.infotec.jbee.core.builder;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.infotec.jbee.core.annotations.History;
import br.com.infotec.jbee.core.reader.HistoryReader;
import br.com.infotec.jbee.core.steps.Configurator;
import br.com.infotec.jbee.core.steps.language.StepsLanguageConfigurator;

/**
 * Build the history from a file from a given test class.
 * 
 * @author Carlos Alberto (euprogramador@gmail.com)
 * @since 1.0
 */
public class HistoryBuilder {

	List<String> historyString = new ArrayList<String>();
	StepsLanguageConfigurator languageSteps;

	
	/**
	 * Configura a classe de teste sobre a qual ser� construida a hist�ria
	 * 
	 * @param testClass
	 * @return HistoryBuilder
	 */
	public HistoryBuilder forTestClass(Class<?> testClass) {
		try {
			History storyAnnotation = testClass.getAnnotation(History.class);
			languageSteps = storyAnnotation.language().newInstance();
			this.historyString = new ArrayList<String>();
			for (String resource : storyAnnotation.value()) {
				InputStream in = testClass.getResourceAsStream(resource);
				this.historyString.addAll(new HistoryReader().readFile(in));
			}
			return this;
		} catch (Exception e) {
			throw new RuntimeException(null, e);
		}
	}

	/**
	 * Build the history
	 * @return HistoryFile
	 */
	public HistoryFile build() {
		HistoryFile history = new HistoryFile();
		for (String line : this.historyString) {
			Step step = null;
			for (Configurator configurator : languageSteps.getLanguageConfigurator()) {
				if (isConfigurator(configurator,line)){
					step = configurator.configure(line);
					break;
				}
			}
			if (step == null){
				step = new TextualStep(line);
			}
			history.addStep(step);
		}
		return history;
	}
	
	private boolean isConfigurator(Configurator configurator,String line){
		Pattern pattern = configurator.getPattern();
		Matcher matcher = pattern.matcher(line);
		return matcher.matches();
	}
}
