/***
 * Copyright (c) 2010 Infotec - http://sites.google.com/site/jbeetest/ All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package br.com.infotec.jbee.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import br.com.infotec.jbee.core.steps.language.DefaultStepsLanguage;
import br.com.infotec.jbee.core.steps.language.StepsLanguageConfigurator;

/**
 * 
 * Configures a class to be tested as an archive of history in a particular
 * language.
 * 
 * @author Carlos Alberto (euprogramador@gmail.com)
 * @since 1.0
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target( { ElementType.TYPE })
public @interface History {

	String[] value();

	Class<? extends StepsLanguageConfigurator> language() default DefaultStepsLanguage.class;
}
