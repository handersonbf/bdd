/***
 * Copyright (c) 2010 Infotec - http://sites.google.com/site/jbeetest/ All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package br.com.infotec.jbee.core.steps.language;

import java.util.ArrayList;
import java.util.List;

import br.com.infotec.jbee.core.steps.Configurator;
import br.com.infotec.jbee.core.steps.ExecutableStepConfigurator;
/**
 * Class that represents the default language of JBee framework (english)
 * 
 * @author Carlos Alberto (euprogramador@gmail.com)
 * @since 1.0
 */
public class DefaultStepsLanguage implements StepsLanguageConfigurator {
	
	public List<Configurator> getLanguageConfigurator(){
		List<Configurator> steps = new ArrayList<Configurator>();
		steps.add(new ExecutableStepConfigurator("Given"));
		steps.add(new ExecutableStepConfigurator("When"));
		steps.add(new ExecutableStepConfigurator("Then"));
		steps.add(new ExecutableStepConfigurator("And"));
		return steps;
	}
	
}