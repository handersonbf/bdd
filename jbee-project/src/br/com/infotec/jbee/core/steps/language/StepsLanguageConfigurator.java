/***
 * Copyright (c) 2010 Infotec - http://sites.google.com/site/jbeetest/ All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package br.com.infotec.jbee.core.steps.language;

import java.util.List;

import br.com.infotec.jbee.core.steps.Configurator;

/**
 * Interface to be implemented by all who wish to make an implementation of a
 * new language for the framework can interpret the file history
 * 
 * @author Carlos Alberto (euprogramador@gmail.com)
 * @since 1.0
 */
public interface StepsLanguageConfigurator {

	List<Configurator> getLanguageConfigurator();

}