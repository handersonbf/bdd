/***
 * Copyright (c) 2010 Infotec - http://sites.google.com/site/jbeetest/ All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package br.com.infotec.jbee.core.junit.executor;

import java.lang.annotation.Annotation;

import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunNotifier;

import br.com.infotec.jbee.core.annotations.WithSpring;
import br.com.infotec.jbee.core.builder.HistoryFile;
import br.com.infotec.jbee.core.builder.Step;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Execute for history
 * 
 * @author Carlos Alberto (euprogramador@gmail.com)
 * @since 1.0
 */
public class HistoryExecutor {

	private Class<?> testClass;
	private RunNotifier notifier;
	private Description description;

	public HistoryExecutor forTestClass(Class<?> testClass) {
		this.testClass = testClass;
		return this;
	}

	public HistoryExecutor forNotifier(RunNotifier notifier) {
		this.notifier = notifier;
		return this;
	}

	public HistoryExecutor forDescription(Description description) {
		this.description = description;
		return this;
	}

	public void executeHistory(HistoryFile history) {
		Object instanceForTesting = obtainInstanceForTest(testClass);
		boolean permitExecutation = true;
		for (Step step : history.getSteps()) {
			step.printDescription();
			if (permitExecutation) {
				try {
					permitExecutation = step.execute(instanceForTesting);
				} catch (Exception e) {
					permitExecutation = false;
					notifier.fireTestFailure(new Failure(description, e));
				}
			}
			step.printStatus();
		}
	}

	private Object obtainInstanceForTest(Class<?> classForTest) {
		try {
			Annotation annotation = classForTest.getAnnotation(WithSpring.class);
			if (annotation!=null) {
				String[] contextPaths = new String[] {"META-INF/applicationContext.xml"};
				ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(contextPaths);
				return context.getBean(classForTest);
			} else {
				return classForTest.newInstance();
			}
		} catch (Exception e) {
			throw new RuntimeException(
					"cannot public constructor is found in test class.",e);
		}
	}

}
