/***
 * Copyright (c) 2010 Infotec - http://sites.google.com/site/jbeetest/ All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package br.com.infotec.jbee.core.evaluation;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class responsible for retrieving the values of the parameters and test
 * whether a value equal to the other even when it contains parameters
 * 
 * @author Carlos Alberto (euprogramador@gmail.com)
 * @since 1.0
 */
public class Evaluation {

	/**
	 * Verify is steps is equals independent if contain parameters
	 * 
	 * @param name1
	 * @param name2
	 * @return boolean 
	 */
	public boolean isEqualsStep(String name1, String name2) {
		Pattern n = Pattern.compile("\\Q#{\\E[a-zA-Z0-9]*\\Q}\\E");
		Matcher a = n.matcher(name1);
		Pattern p = Pattern.compile("\\Q" + a.replaceAll("\\\\E.+\\\\Q")
				+ "\\E");
		Matcher m = p.matcher(name2);
		return m.matches();
	}


	/**
	 * Retrive parameters for steps
	 * 
	 * @param name1
	 * @param name2
	 * @return Object[] list of values for argument
	 */
	public Object[] getArguments(String name1, String name2) {
		Pattern keyPattern = Pattern.compile("\\Q#{\\E[a-zA-Z0-9]*\\Q}\\E");
		Matcher keyMatcher = keyPattern.matcher(name1);

		String newKey = keyMatcher.replaceAll("#{}#");
		String[] tokens = newKey.split("#");
		List<String> arguments = new ArrayList<String>();
		for (int i = 0; i < tokens.length; i++) {
			if (tokens[i].equals("{}")) {
				int start = 0;
				int end = name2.length();

				start = name2.indexOf(tokens[i - 1])
						+ tokens[i - 1].length();
				if (i + 1 < tokens.length)
					end = name2.indexOf(tokens[i + 1]);
				arguments.add(name2.substring(start, end));
			}
		}
		return arguments.toArray();
	}

}
