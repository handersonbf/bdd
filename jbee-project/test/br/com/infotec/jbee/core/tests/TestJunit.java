package br.com.infotec.jbee.core.tests;

import junit.framework.Assert;

import org.junit.runner.RunWith;

import br.com.infotec.jbee.core.annotations.Step;
import br.com.infotec.jbee.core.annotations.History;
import br.com.infotec.jbee.core.junit.runner.JBeeRunner;
import br.com.infotec.jbee.core.steps.language.PtBRStepsLanguage;

@RunWith(JBeeRunner.class)
@History(value="registroClienteNovo.history",language=PtBRStepsLanguage.class)
public class TestJunit {

	@Step("Dado que estou na tela de listagem de clientes")
	public void entrarTelaListagemClientes() {
	}

	@Step("Quando preencho no nome #{nome} e salvo")
	public void preencheInformacoesESalva(String nome) {
		Assert.assertEquals("Carlos Alberto", nome);
	}

	@Step("Ent�o o sistema apresenta o novo cliente na lista de clientes")
	public void testaSeEstaApresentandoClienteNovoNaLista() {
	}

	@Step("Quando preencho as informa��es sem o nome e tento salvar")
	public void preencheInformacoesSemNomeETentaSalvar() {
	}

	@Step("Ent�o o sistema apresenta uma mensagem de erro")
	public void verificaSeApresentouMensagemDeErro(){
	}
	
	@Step("Quando seleciono o cliente e escolho excluir")
	public void quandoSelecionoClienteEOExcluo(){
	}
	
	@Step("Ent�o o cliente � apagado")
	public void testaSeClienteFoiApagado(){
	}

}
