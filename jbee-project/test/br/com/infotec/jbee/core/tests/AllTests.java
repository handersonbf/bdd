package br.com.infotec.jbee.core.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.infotec.jbee.core.tests.ne.TestNova;

@RunWith(Suite.class)
@Suite.SuiteClasses( { TestJunit.class,TestNova.class })
public class AllTests {


}
