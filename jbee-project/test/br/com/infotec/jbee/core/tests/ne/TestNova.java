package br.com.infotec.jbee.core.tests.ne;

import org.junit.runner.RunWith;

import br.com.infotec.jbee.core.annotations.History;
import br.com.infotec.jbee.core.annotations.Step;
import br.com.infotec.jbee.core.junit.runner.JBeeRunner;
import br.com.infotec.jbee.core.steps.language.PtBRStepsLanguage;

@RunWith(JBeeRunner.class)
@History(value="novaHistoria.history",language=PtBRStepsLanguage.class)
public class TestNova {

	@Step("Dado que estou testando a suite")
	public void quandoSelecionoClienteEOExcluo1(){
	}
	
	
	@Step("Quando eu executar os testes")
	public void quandoSelecionoClienteEOExcluo2(){
	}
	
	@Step("Ent�o tudo rodar� sem problemas")
	public void testaSeClienteFoiApagado(){
	}

}
